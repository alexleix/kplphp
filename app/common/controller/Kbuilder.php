<?php
namespace app\common\controller;

/*
 * Kbuilder快速构建器
 */
class Kbuilder {

    protected static $vars = [];
    public function initialize()
    {

    }

    /**
     * kplphp构建器
     * @param string $t == table 表格构建器 ，== form 表单构建器
     * @return mixed
     */
    public static function sets($t='table')
    {
        if($t == 'table')
        {
            $class = \app\common\kbuilder\table\Kbuilder::class;
        }elseif($t == 'see')
        {
            $class = \app\common\kbuilder\see\Kbuilder::class;
        }else{
            $class = \app\common\kbuilder\form\Kbuilder::class;
        }
        return new $class;
    }


}